# Note Taking tools for eDIVE

This module adds note-taking functionality to eDIVE. See the thesis for more details https://is.muni.cz/th/udwb7/.

Developed by Tereza Tódová (FI MUNI) and Vojtěch Brůža (FI MUNI).

## License
This software is licensed under the [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html). See Licence.md for full text of licence.
