﻿// Created Vojtech Bruza
using UnityEngine;

namespace Edive.NoteTakingTools.Drawing
{
    /// <summary>
    /// This is just a temporary class before we refactor this NoteTaking.
    /// </summary>
    public static class LineObjectUtils
    {
        public static bool Highlight(bool drawingInProgress, float width, LineRenderer lineRenderer, 
                                    Color color, float colorSaturationTresh, float colorValue)
        {
            if (drawingInProgress) return false;
            // when highlighting the object it gets bigger and lighter
            var w3 = width * 3f;
            lineRenderer.startWidth = w3;
            lineRenderer.endWidth = w3;

            // the color on the private object is already "muted" in comparison to the public objects 
            float h, s, v;
            Color.RGBToHSV(color, out h, out s, out v);
            // we change the saturation of the colorfull objects and the value of the black color
            if (s > colorSaturationTresh)
                s = s / 2;
            else
                v = colorValue;
            var newColor = Color.HSVToRGB(h, s, v);
            lineRenderer.material.color = newColor;

            return true;
        }

        public static bool StopHighlight(bool drawingInProgress, LineRenderer lineRenderer, float width, Color color)
        {
            if (drawingInProgress) return false;
            lineRenderer.startWidth = width;
            lineRenderer.endWidth = width;
            lineRenderer.material.color = color;
            return true;
        }
    }
}